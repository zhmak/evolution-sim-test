
data class Stats(val fighters: List<Fighter>) {
    val averageHeadSize = fighters.map { it.headSize }.average()
    val averageTorsoSize = fighters.map { it.torsoSize }.average()
    val averageArmsSize = fighters.map { it.armsSize }.average()
    val averageLegsSize = fighters.map { it.legsSize }.average()
    val averageBodySize = fighters.map {it.bodySize }.average()

    override fun toString(): String {
        return  "averageHeadSize=$averageHeadSize\n" +
                "averageTorsoSize=$averageTorsoSize\n" +
                "averageArmsSize=$averageArmsSize\n" +
                "averageLegsSize=$averageLegsSize\n" +
                "averageBodySize=$averageBodySize\n"
    }
}