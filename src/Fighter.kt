
private const val baseHealth: Double = 100.0
private const val baseDamage: Double = 10.0
private const val baseAccuracy: Double = 0.8
private const val baseCritChance: Double = 0.05
private const val baseDodgeChance: Double = 0.25
private const val baseInitiative: Double = 1.0

private fun average(vararg args: Double): Double {
    return args.average()
}

class Fighter(val headSize: Double, val torsoSize: Double, val armsSize: Double, val legsSize: Double) {
    val health: Double
    val damage: Double
    val accuracy: Double
    val initiative: Double
    val critChance: Double
    val dodgeChance: Double

    val bodySize = average(headSize * 0.5, torsoSize * 1.35, armsSize, legsSize * 1.15)
    init {
        health = if (baseHealth * bodySize > 1000) 1000.0 else baseHealth * bodySize
        initiative = baseInitiative / bodySize
        damage = baseDamage * average(torsoSize , armsSize * 1.5, legsSize * 0.5)
        accuracy = baseAccuracy / average(bodySize * 0.5, armsSize * 0.5)
        critChance = baseCritChance * headSize
        dodgeChance = baseDodgeChance / average(headSize * 2.0, torsoSize * 0.75, armsSize * 0.25)
    }
}