import kotlin.random.Random

private val rng = Random

private fun tryAttack(attacker: Fighter, defender: Fighter, defenderHp: Double): Double {
    return if (rng.nextDouble(1.0) < attacker.accuracy && rng.nextDouble(1.0) > defender.dodgeChance) {
        val damage = rng.nextDouble(0.75, 1.25) * attacker.damage
        defenderHp - (damage * if (rng.nextDouble(1.0) < attacker.critChance) 3.0 else 1.0)
    } else
        defenderHp
}

fun simulateFight(first: Fighter,  second: Fighter): Fighter {
    tailrec fun loop (firstHp: Double, secondHp: Double): Fighter {
        if (rng.nextDouble(1.0) < first.initiative / second.initiative) {
            val newSecondHp = tryAttack(first, second, secondHp)
            if (newSecondHp <= 0.0) return first
            val newFirstHp = tryAttack(second, first, firstHp)
            if (newFirstHp <= 0.0) return second
            return loop(newFirstHp, newSecondHp)
        } else {
            val newFirstHp = tryAttack(second, first, firstHp)
            if (newFirstHp <= 0.0) return second
            val newSecondHp = tryAttack(first, second, secondHp)
            if (newSecondHp <= 0.0) return first
            return loop(newFirstHp, newSecondHp)
        }
    }
    return loop(first.health, second.health)
}