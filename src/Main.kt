import java.lang.Exception
import kotlin.random.Random

private val rng = Random

fun createFirstGeneration(size: Int): MutableList<Fighter> {
    return (0 until size).map {
        Fighter(
            rng.nextDouble(0.9, 1.1),
            rng.nextDouble(0.9, 1.1),
            rng.nextDouble(0.9, 1.1),
            rng.nextDouble(0.9, 1.1)
        )}.toMutableList()
}


fun pickFighterPairs(fighters: MutableList<Fighter>): List<Pair<Fighter, Fighter>> {
    if (fighters.size % 2 != 0) throw Exception("Provide even sized list of fighters")
    val pairs = ArrayList<Pair<Fighter, Fighter>>()
    for (i in 0 until (fighters.size / 2)) {
        pairs.add(Pair(
            fighters.removeAt(rng.nextInt(fighters.size)),
            fighters.removeAt(rng.nextInt(fighters.size))
        ))
    }
    return pairs
}

const val mutationChance = 0.2
fun generateOffspring(first: Fighter, second: Fighter, size: Int): List<Fighter> {
    fun tryMutate(size: Double): Double {
        return if (rng.nextDouble(1.0) < mutationChance)
            rng.nextDouble(0.9, 1.1) * size
        else
            size
    }

    return (0 until size).map {
        Fighter(
            tryMutate((first.headSize + second.headSize) / 2),
            tryMutate((first.torsoSize + second.torsoSize) / 2),
            tryMutate((first.armsSize + second.armsSize) / 2),
            tryMutate((first.legsSize + second.legsSize) / 2)
        )
    }.toList()
}

fun simulateNaturalSelection(firstGen: MutableList<Fighter>, lastGen: Int): List<Fighter> {
    tailrec fun loop(newGen: MutableList<Fighter>, currGen: Int): List<Fighter> {
        if (currGen % 100 == 0) println("Gen $currGen statistics:\n${(Stats(newGen))}\n")
        if (currGen == lastGen) return newGen
        val survivors = pickFighterPairs(newGen)
            .map { simulateFight(it.first, it.second) }.toMutableList()
        val offspring = pickFighterPairs(survivors)
            .map { generateOffspring(it.first, it.second, 4) }
            .flatten().toMutableList()
        return loop(offspring, currGen + 1)
    }
    return loop(firstGen, 1)
}

fun main(args: Array<String>) {
    val gen10k = simulateNaturalSelection(createFirstGeneration(10000), 100000)
}